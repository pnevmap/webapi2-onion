# README #

### Summary ###
Implemented Rest api functionality for the essential domain (Customer, Order).

The onion architecture used in order to achieve loose coupling and unit testing. 

Infrastructure implementation uses WebAPI2 with EF6 (code first) on sql server.

### Functionality ###

[GET] V1/Customers

Returns a list of customers, without orders

[GET] V1/Customers/{id}

Returns customer and his order list 

[POST] /V1/Customers/{id} 

Add new Order for existing Customer 

[POST] /V1/Customers 

Add new Customer

*The response is in both json and xml format* [the default format is Json, if used the querystring parameter type=xml then the result is in xml format]


### How do I get set up? ###
* Just Clone or Download the repository and open the .sln file with visual studio. 


* Restore nuget packages for the solution


* Set the web application project "SrAssignment.WebAPI" as startup project and run! (The application requires local sql server)


* During the first run the application itself creates the database!


* (The database can be created by running the Run_This_To_Init_DB.CreateDatabase "unit test")


### Architecture ###



The **[Onion architecture](http://jeffreypalermo.com/blog/the-onion-architecture-part-1/)** has been followed as shown bellow (For simplicity reasons the Dependency Resolution assembly has been merged with the web project. The project dependency graph is shown at the end of the "read me")


![onion.png](https://bitbucket.org/repo/LgAMn7/images/2921258612-onion.png)


### Key Frameworks ###
In the Infrastructure have been used the following frameworks


* .net 4.5, WebAPI2
 
* ORM: **EF6** (code first approch)
 
* IoC Container: **Ninject** 
 
* Object-object Mapper: **AutoMapper**
 
* Testing: **NUnit3**
 
* Mocking Framework: **RhinoMocks**
 
* Fake data: **Faker.net**

### Project Dependencies Graph ###


![Dependencies Graph.png](https://bitbucket.org/repo/LgAMn7/images/1306511398-Dependencies%20Graph.png)

The unit test project has been excluded from the graph