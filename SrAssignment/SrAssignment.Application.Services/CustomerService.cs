﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SrAssignment.Core.Domain;
using SrAssignment.Core.Domain.Services;

namespace SrAssignment.Application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }
        public ICollection<Customer> All()
        {
            return _customerRepository.All();
        }

        public Customer Get(int Id)
        {
            return _customerRepository.Get(Id);
        }
        public virtual Customer Create(Customer customer)
        {
            return _customerRepository.Create(customer);
        }

        public Order CreateOrder(Order order)
        {
            return _customerRepository.CreateOrder(order);
        }
    }
}
