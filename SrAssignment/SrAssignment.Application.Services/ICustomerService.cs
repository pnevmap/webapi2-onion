﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SrAssignment.Core.Domain;

namespace SrAssignment.Application.Services
{
    public interface ICustomerService
    {
        ICollection<Customer> All();
        Customer Get(int id);
        Customer Create(Customer customer);
        Order CreateOrder(Order order);

    }
}
