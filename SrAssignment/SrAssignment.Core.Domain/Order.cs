﻿using System;

namespace SrAssignment.Core.Domain
{
    public class Order
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public Decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        
    }
}
