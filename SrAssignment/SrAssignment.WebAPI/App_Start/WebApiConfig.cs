﻿using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Web.Http;
using SrAssignment.WebAPI.Dto;

namespace SrAssignment.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services


            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Formatters.XmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("type", "xml",
                new MediaTypeHeaderValue("text/xml")));
            var xml = GlobalConfiguration.Configuration.Formatters.XmlFormatter;
            /* preserveObjectReferences: */
            var ordersSerializer = new DataContractSerializer(typeof(Order), null, int.MaxValue, false, true, null);
            xml.SetSerializer<Order>(ordersSerializer);
            var customersSerializer = new DataContractSerializer(typeof(Customer), null, int.MaxValue, false, true, null);
            xml.SetSerializer<Customer>(customersSerializer);

            config.Routes.MapHttpRoute("DefaultApi", "V1/{controller}/{id}", new { id = RouteParameter.Optional });
        }
    }
}