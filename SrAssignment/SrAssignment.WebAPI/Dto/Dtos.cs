﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
namespace SrAssignment.WebAPI.Dto
{

    [DataContract]
    public class Customer
    {
        [DataMember]
        [ReadOnly(true)]
        public int Id { get; set; }

        [DataMember]
        [MaxLength(255)]
        public string Name { get; set; }

        [DataMember]
        [MaxLength(255)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        public ICollection<Order> Orders { get; set; }
        public bool ShouldSerializeOrders()
        {
            return Orders == null || Orders.Count > 0 ;
        }
    }

    [DataContract(IsReference = true)]
    public class Order
    {
        [DataMember]
        [ReadOnly(true)]
        public int Id { get; set; }

        [DataMember]
        public int? CustomerId { get; set; }

        [DataMember]
        public Customer Customer { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

    }
}