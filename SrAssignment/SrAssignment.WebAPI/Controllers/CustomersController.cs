﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using SrAssignment.Application.Services;
using SrAssignment.WebAPI.Dto;

namespace SrAssignment.WebAPI.Controllers
{
    public class CustomersController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        public IEnumerable<Customer> GetCustomers()
        {
            return _customerService.All().ToList()
                .ConvertAll(c => _mapper.Map<Customer>(c));
        }

        [ResponseType(typeof(Customer))]
        public IHttpActionResult GetCustomers([FromUri] int id)
        {
            var customer = _customerService.Get(id);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<Customer>(customer));
        }


        [ResponseType(typeof(Customer))]
        [ValidateModel]
        public IHttpActionResult PostCustomer(Customer customer)
        {
            var customer2Save = _mapper.Map<Core.Domain.Customer>(customer);

            _customerService.Create(customer2Save);

            return CreatedAtRoute("DefaultApi", new { id = customer2Save.Id }, _mapper.Map<Customer>(customer2Save));
        }
        /// <summary>
        /// fill only CustomerId and Price
        /// </summary>
        /// <param name="id"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        [ResponseType(typeof(Order))]
        [ValidateModel]
        public IHttpActionResult PostOrder([FromUri]int id, [FromBody]Order order)
        {
            if (id != order.CustomerId)
                return BadRequest();

            if (!order.CustomerId.HasValue)
                order.CustomerId = id;

            if (!order.CreatedDate.HasValue)
                order.CreatedDate = DateTime.Now;

            var order2Save = _mapper.Map<Core.Domain.Order>(order);

            _customerService.CreateOrder(order2Save);

            return CreatedAtRoute("DefaultApi", new { id = order2Save.Id }, _mapper.Map<Order>(order2Save));
        }
    }
}