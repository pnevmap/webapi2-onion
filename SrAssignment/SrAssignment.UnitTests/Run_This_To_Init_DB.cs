﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using NUnit.Framework;
using Rhino.Mocks;
using SrAssignment.Application.Services;
using SrAssignment.Core.Domain;
using SrAssignment.Core.Domain.Services;
using SrAssignment.Infrastructure.Data;

namespace SrAssignment.UnitTests
{
    [TestFixture]
    public class Run_This_To_Init_DB
    {
        private static readonly StandardKernel _kernel = new StandardKernel();

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            _kernel.Dispose();
        }

        [Test]
        public void CreateDatabase()
        {
            DataContext db = new DataContext();
            var orders = db.Orders.FirstOrDefault();
        }

    }
}
