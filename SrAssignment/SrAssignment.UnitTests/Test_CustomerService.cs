﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using NUnit.Framework;
using Rhino.Mocks;
using SrAssignment.Application.Services;
using SrAssignment.Core.Domain;
using SrAssignment.Core.Domain.Services;

namespace SrAssignment.UnitTests
{
    [TestFixture]
    public class Test_CustomerService
    {
        private static readonly StandardKernel _kernel = new StandardKernel();
        private ICustomerRepository _customerRepository;
        private ICustomerService _customerService;
        private readonly List<Customer> _expectedCustomers = TestDataHelper.Customers(15);

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _kernel.Bind<ICustomerService>().To<CustomerService>();
            _customerRepository = MockRepository.GenerateMock<ICustomerRepository>();
            _kernel.Bind<ICustomerRepository>().ToConstant(_customerRepository);
            _customerService = _kernel.Get<ICustomerService>();
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            _kernel.Dispose();
        }

        [Test]
        public void CustomerService_All_CallRepositoryAll()
        {
            //Arrange
            //Act
            ICollection<Customer> customersFromService = _customerService.All();

            //Assert
            _customerRepository.AssertWasCalled(cr => cr.All());
        }

        [Test]
        public void CustomerService_Get_CallRepository_Get([Random(1, 100, 10)] int id)
        {
            //Arrange
            //Act
            Customer customerFromService = _customerService.Get(id);

            //Assert
            _customerRepository.AssertWasCalled(cr => cr.Get(id));
        }

        [Test]
        public void CustomerService_All()
        {
            //Arrange
            _customerRepository.Stub(m => m.All()).Return(_expectedCustomers);
            //Act
            ICollection<Customer> customersFromService = _customerService.All();

            //Assert
            Assert.AreSame(_expectedCustomers, customersFromService);
        }

        [Test]
        public void CustomerService_Get_Found([Range(0, 15)] int id)
        {
            //Arrange
            Customer expectedCustomer = _expectedCustomers.FirstOrDefault(c => c.Id == id);
            _customerRepository.Stub(m => m.Get(id)).Return(expectedCustomer);
            
            //Act
            Customer customerFromService = _customerService.Get(id);

            //Assert
            Assert.AreSame(expectedCustomer, customerFromService);
            

        }

        [Test]
        public void CustomerService_Get_NotFound([Range(0, 15)] int id)
        {
            //Arrange
            _customerRepository.ClearBehavior();
            _customerRepository.Stub(m => m.Get(id)).Return(null);

            //Act
            Customer customerFromService = _customerService.Get(id);

            //Assert
            Assert.IsNull(customerFromService);


        }
    }
}
