﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rhino.Mocks;

namespace SrAssignment.UnitTests
{
    public static class RhinoExtensions
    {
        /// <summary>
        /// Clears the behavior already recorded in a Rhino Mocks stub.
        /// </summary>
        public static void ClearBehavior<T>(this T stub)
        {
            stub.BackToRecord(BackToRecordOptions.All);
            stub.Replay();
        }

        public static IDbSet<T> MockToDbSet<T>(this IQueryable<T> queryable) where T : class
        {
            IDbSet<T> mockDbSet = MockRepository.GenerateMock<IDbSet<T>>();
            mockDbSet.Stub(m => m.Provider).Return(queryable.Provider);
            mockDbSet.Stub(m => m.Expression).Return(queryable.Expression);
            mockDbSet.Stub(m => m.ElementType).Return(queryable.ElementType);
            mockDbSet.Stub(m => m.GetEnumerator()).Return(queryable.GetEnumerator());
            return mockDbSet;
        }
    }
}
