﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using Ninject;
using NUnit.Framework;
using Rhino.Mocks;
using SrAssignment.Application.Services;
using SrAssignment.WebAPI.Controllers;
using AutoMapper;
using System.Web.Http;
using System.Net.Http;
using System;
using System.Web.Http.Routing;
using Newtonsoft.Json.Linq;
using SrAssignment.Core.Domain;

namespace SrAssignment.UnitTests
{
    [TestFixture]
    public class Test_CustomersController
    {
        private static readonly StandardKernel _kernel = new StandardKernel();
        private ICustomerService _customerService;
        private readonly List<Core.Domain.Customer> _dummyCustomers = TestDataHelper.Customers(15);
        private IMapper _mapper;
        private CustomersController _controller;
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _customerService = MockRepository.GenerateMock<ICustomerService>();
            _kernel.Bind<ICustomerService>().ToConstant(_customerService);
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Core.Domain.Order, WebAPI.Dto.Order>().ReverseMap();
                cfg.CreateMap<Core.Domain.Customer, WebAPI.Dto.Customer>().ReverseMap();
            });
            _mapper = config.CreateMapper();
            _kernel.Bind<IMapper>().ToConstant(_mapper);

            _controller = _kernel.Get<CustomersController>();
            _controller.Configuration = new HttpConfiguration();
            _controller.Request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("http://localhost/SrAssignment.WebAPI")
            };
            _controller.Configuration.MapHttpAttributeRoutes();
            _controller.Configuration.EnsureInitialized();
            _controller.RequestContext.RouteData = new HttpRouteData(new HttpRoute(), new HttpRouteValueDictionary { { "controller", "Customers" } });

        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            _kernel.Dispose();
        }

        [Test]
        public void CustomersController_GetCustomers()
        {
            //Arrange
            CustomersController controller = _kernel.Get<CustomersController>();
            _customerService.ClearBehavior();
            _customerService.Stub(m => m.All()).Return(_dummyCustomers);
            //Act
            IEnumerable<WebAPI.Dto.Customer> customersFromController = controller.GetCustomers();
            //Assert
            var expected = _dummyCustomers.ConvertAll(c => _mapper.Map<WebAPI.Dto.Customer>(c)).AsEnumerable();
            Assert.AreEqual(expected.Select(e => e.Id), customersFromController.Select(e => e.Id));
            Assert.AreEqual(expected.Select(e => e.Email), customersFromController.Select(e => e.Email));
            Assert.AreEqual(expected.Select(e => e.Name), customersFromController.Select(e => e.Name));
        }

        [Test]
        public void CustomersController_GetCustomers_Found([Random(0, 15, 10)] int id)
        {
            //Arrange
            Customer expectedCustomer = _dummyCustomers.FirstOrDefault(c => c.Id == id);
            _customerService.ClearBehavior();
            _customerService.Stub(m => m.Get(id)).Return(_dummyCustomers.FirstOrDefault(c => c.Id == id));
            //Act
            WebAPI.Dto.Customer customerFromController = (_controller.GetCustomers(id) as OkNegotiatedContentResult<WebAPI.Dto.Customer>)?.Content;
            //Assert
            Assert.AreEqual(expectedCustomer.Id, customerFromController.Id);
            Assert.AreEqual(expectedCustomer.Email, customerFromController.Email);
            Assert.AreEqual(expectedCustomer.Name, customerFromController.Name);
            Assert.AreEqual(expectedCustomer.Orders.Count, customerFromController.Orders.Count);
            if (expectedCustomer.Orders.Count > 0)
            {
                Assert.AreEqual(expectedCustomer.Orders.FirstOrDefault().CreatedDate, customerFromController.Orders.FirstOrDefault().CreatedDate);
            }
        }


        [Test]
        public void CustomersController_GetCustomers_NotFound([Random(1, 15, 10)] int id)
        {
            //Arrange
            CustomersController controller = _kernel.Get<CustomersController>();
            _customerService.ClearBehavior();
            _customerService.Stub(m => m.Get(id)).Return(null);
            //Act
            var notFoundResultFromController = controller.GetCustomers(id);

            //Assert
            Assert.That(notFoundResultFromController, Is.TypeOf<NotFoundResult>());
        }
        [Test]
        public void CustomerController_PostCustomers_ResultOK()
        {
            //Arrange
            WebAPI.Dto.Customer customer2Post = new WebAPI.Dto.Customer
            {
                Id = Faker.RandomNumber.Next(),
                Name = Faker.Name.FullName(),
                Email = Faker.Internet.Email()
            };

            Core.Domain.Customer customer2Save = _mapper.Map<Core.Domain.Customer>(customer2Post);
            _customerService.Stub(m => m.Create(null)).IgnoreArguments().Return(customer2Save);

            //Act
            var controllerResult = _controller.PostCustomer(customer2Post) as CreatedAtRouteNegotiatedContentResult<WebAPI.Dto.Customer>;

            //Assert

            Assert.IsNotNull(controllerResult);
            Assert.That(controllerResult.RouteName, Is.EqualTo("DefaultApi"));
            Assert.AreEqual(customer2Post.Id, controllerResult.Content.Id);
            Assert.AreEqual(customer2Post.Email, controllerResult.Content.Email);
        }

        [Test]
        public void CustomerController_PostOrder_Result_OK([Range(0, 1)]int customerId, [Random(16.10, 300.50, 1)] decimal price)
        {
            //Arrange
            Order order2Stub = new Order
            {
                Id = Faker.RandomNumber.Next(),
                CustomerId = customerId,
                Price = price,
                CreatedDate = DateTime.Now
            };

            WebAPI.Dto.Order order2Post = _mapper.Map<WebAPI.Dto.Order>(order2Stub);

            _customerService.Stub(m => m.CreateOrder(null)).IgnoreArguments().Return(order2Stub);

            //Act
            var controllerResult = _controller.PostOrder(customerId, order2Post) as CreatedAtRouteNegotiatedContentResult<WebAPI.Dto.Order>;

            //Assert

            Assert.IsNotNull(controllerResult);
            Assert.That(controllerResult.RouteName, Is.EqualTo("DefaultApi"));
            Assert.AreEqual(price, controllerResult.Content.Price);
            Assert.AreEqual(customerId, controllerResult.Content.CustomerId);
        }

        [Test]
        public void CustomerController_PostOrder_Result_BadRequest()
        {
            //Arrange
            WebAPI.Dto.Order orde2Post = new WebAPI.Dto.Order
            {
                CustomerId = 1,
                Price = -1
            };
            _controller.Request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("http://localhost/SrAssignment.WebAPI/V1/Customers/-1")
            };

            var badresult = _controller.PostOrder(-1, orde2Post);
            Assert.That(badresult, Is.TypeOf<BadRequestResult>());
        }
    }
}
