﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Faker;
using SrAssignment.Core.Domain;

namespace SrAssignment.UnitTests
{
    public class TestDataHelper
    {
        public static List<Customer> Customers(int count)
        {
            return Enumerable.Range(0, count).Select(Customer).ToList();
        }

        public static Customer Customer(int id)
        {
            int count = Faker.RandomNumber.Next(10);
            List<Order> customerOrders = Enumerable.Range(0, count).Select(i => Order(i, id)).ToList();
            return new Customer
            {
                Id = id,
                Name = Name.First(),
                Email = Internet.Email(),
                Orders = customerOrders
            };
        }

        public static Order Order(int orderId, int customerId)
        {
            return new Order
            {
                Id = orderId,
                CustomerId = customerId,
                Price = Faker.RandomNumber.Next(),
                CreatedDate = DateTime.Now.AddDays(RandomNumber.Next(355)),
            };
        }
    }
}
