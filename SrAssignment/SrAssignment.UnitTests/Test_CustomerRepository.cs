﻿using NUnit.Framework;
using Ninject;
using Rhino.Mocks;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SrAssignment.Core.Domain;
using SrAssignment.Core.Domain.Services;
using SrAssignment.Infrastructure.Data;
using SrAssignment.Infrastructure.Data.Repositories;
using SrAssignment.UnitTests;

namespace SrAssignment.UnitTests
{

    [TestFixture]
    public class Test_CustomerRepository
    {
        private static readonly StandardKernel _kernel = new StandardKernel();
        private DataContext _db;
        private ICustomerRepository _customerRepository;
        private readonly IQueryable<Customer> _dummyCustomers = TestDataHelper.Customers(15).AsQueryable();

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _db = MockRepository.GenerateMock<DataContext>();
            _kernel.Bind<DataContext>().ToConstant(_db);
            _kernel.Bind<ICustomerRepository>().To<CustomerRepository>();
            _db.Stub(p => p.Customers).Return(_dummyCustomers.MockToDbSet());
            _customerRepository = _kernel.Get<ICustomerRepository>();
        }

        [Test]
        public void CustomerRepository_All()
        {
            //Arrange
            List<Customer> expectedCustomers = _dummyCustomers.ToList();
            //Act
            ICollection<Customer> customersFromRepository = _customerRepository.All();
            //Assert
            Assert.AreEqual(expectedCustomers, customersFromRepository);
        }

        [Test]
        public void CustomerRepository_Get_Found([Random(0, 15, 10)] int id)
        {
            //Arrange
            Customer expectedCustomer = _dummyCustomers.FirstOrDefault(c => c.Id == id);
            //Act
            Customer customerFromRepository = _customerRepository.Get(id);
            //Assert
            Assert.AreEqual(expectedCustomer, customerFromRepository);
        }

        [Test]
        public void CustomerRepository_Get_NotFound([Random(50, 100, 10)] int id)
        {
            //Arrange
            IQueryable<Customer> emptyCustomers = new List<Customer>().AsQueryable();
            _db.ClearBehavior();
            _db.Stub(p => p.Customers).Return(emptyCustomers.MockToDbSet());

            //Act
            Customer customerFromRepository = _customerRepository.Get(id);
            //Assert
            Assert.IsNull(customerFromRepository);
        }

    }
}
