﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SrAssignment.Core.Domain;
using SrAssignment.Core.Domain.Services;

namespace SrAssignment.Infrastructure.Data.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DataContext _context;

        public CustomerRepository(DataContext context)
        {
            _context = context;
        }

        public Customer Get(int id)
        {
            return _context.Customers.Include("Orders").FirstOrDefault(c => c.Id == id);
        }

        public ICollection<Customer> All()
        {
            return _context.Customers.ToList();
        }

        public Customer Create(Customer customer)
        {
            _context.Customers.Add(customer);
            _context.SaveChanges();
            return customer;
        }

        public Order CreateOrder(Order order)
        {
            _context.Orders.Add(order);
            _context.SaveChanges();
            return order;

        }
    }
}
