﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faker;
using SrAssignment.Core.Domain;

namespace SrAssignment.Infrastructure.Data.Migrations
{
    public class DummyDataHelper
    {
        public static List<Customer> Customers(int count) => Enumerable.Range(0, count).Select(i => Customer()).ToList();

        public static Customer Customer()
        {
            return new Customer
            {
                Name = Name.First(),
                Email = Internet.Email(),
                Orders = Enumerable.Range(0, Faker.RandomNumber.Next(10)).Select(i => Order()).ToList()
            };
        }

        public static Order Order()
        {
            return new Order
            {
                Price = Faker.RandomNumber.Next(),
                CreatedDate = DateTime.Now.AddDays(RandomNumber.Next(355)),
            };
        }

    }
}
