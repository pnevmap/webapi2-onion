using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using SrAssignment.Core.Domain;

namespace SrAssignment.Infrastructure.Data.Migrations
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            ToTable("Customer");
            HasKey(c => c.Id);
            Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.Email).HasMaxLength(255);
            Property(c => c.Name).HasMaxLength(255);
            HasMany(o => o.Orders).WithOptional(o => o.Customer).HasForeignKey(o => o.CustomerId);
        }
    }
}