using System.Data.Entity;
using System.Data.Entity.Migrations;
using SrAssignment.Core.Domain;

namespace SrAssignment.Infrastructure.Data.Migrations
{
    public class CreateSampleSeedInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            context.Customers.AddOrUpdate(DummyDataHelper.Customers(15).ToArray());
            context.SaveChanges();
            base.Seed(context);
        }
    }
}