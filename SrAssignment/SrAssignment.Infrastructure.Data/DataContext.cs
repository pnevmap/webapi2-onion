﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SrAssignment.Core.Domain;
using SrAssignment.Infrastructure.Data.Migrations;


namespace SrAssignment.Infrastructure.Data
{
    public class DataContext : DbContext
    {
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<Customer> Customers { get; set; }
        public DataContext() : base("SrAssignmentDB")
        {
            Configuration.LazyLoadingEnabled = false;
            if (!Database.Exists("SrAssignmentDB"))
                Database.SetInitializer(new CreateSampleSeedInitializer());
            else
                Database.SetInitializer(new ChangeSampleSeedInitializer());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new CustomerConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
