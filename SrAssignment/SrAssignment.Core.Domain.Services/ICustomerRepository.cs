﻿using System.Collections.Generic;

namespace SrAssignment.Core.Domain.Services
{
    public interface ICustomerRepository
    {
        ICollection<Customer> All();
        Customer Get(int id);
        Customer Create(Customer customer);
        Order CreateOrder(Order order);
    }
}
